class Main():
    def __init__(self):
        self.pi = 3.1416

    def calVolume(self, d, h):
        area = Main.calArea(d)
        volume = area * h 
        return {'La circunferencia del cilindro tiene un area de': area,'y un Volumen de': volume }


    def calArea(d):
        area = 3.1416 * ((d/2)**2)
        return area



if __name__ == "__main__":

    mainClass = Main()

    while True:
        try:
            diametro = float(input("Ingrese el Diametro: "))
            altura = float(input("Ingrese el Altura: "))
            break
        except:
            print("Error: Invalid argument.")

    print(mainClass.calVolume(diametro, altura))




    


