Autor: David A. Diaz

-----------------INDICE DE PROGRAMAS-------------------

1.Escribir un programa que pida al usuario una palabra y la muestre por pantalla 10 veces.
2.Escribir un programa que pida al usuario un número entero positivo y muestre por pantalla todos los números impares desde 1 hasta ese número separados por comas.
3.Escribir una función que muestre por pantalla el saludo ¡Hola amiga! cada vez que se la invoque.
4.Escribir una función a la que se le pase una cadena <nombre> y muestre por pantalla el saludo ¡hola <nombre>!.
5.Escribir una función que calcule el área de un círculo y otra que calcule el volumen de un cilindro usando la primera función.
6.Escribir una función que reciba una muestra de números en una lista y devuelva su media.
7.Escribir un programa que pregunte al usuario su edad y muestre por pantalla si es mayor de edad o no.
8.Escribir un programa que almacene la cadena de caracteres contraseña en una variable, pregunte al usuario por la contraseña e imprima por pantalla si la contraseña introducida por el usuario coincide con la guardada en la variable sin tener en cuenta mayúsculas y minúsculas.
9.Escribir un programa que pida al usuario dos números y muestre por pantalla su división. Si el divisor es cero el programa debe mostrar un error.
10.Escribir un programa que pida al usuario un número entero y muestre por pantalla si es par o impar.
