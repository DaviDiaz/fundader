#Nombre: David Diaz
#Cedula: 8-930-2006

from modulo import PaymentGateway

pg = PaymentGateway()

if __name__ == "__main__":
    transaction = input('Tipo de transaccion: ')
    mount = float(input("Monto: "))
    card = input('ID Card: ')

    resultado = pg.do_sale(transaction=transaction, cost=mount, card=card)

    print(resultado)
