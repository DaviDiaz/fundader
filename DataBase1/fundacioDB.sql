CREATE TABLE alumno (
	id INT PRIMARY KEY AUTO_INCREMENT,
	nombre_alum VARCHAR(30) NOT NULL,
	cedula_alum VARCHAR(30) NOT NULL,
	telefono_alum VARCHAR(20),
	fecha_ing_alumno DATE NOT NULL,
	correo_alum VARCHAR(50),
	turno_alum VARCHAR(30) NOT NULL,
	grupo INT NOT NULL
);

CREATE TABLE sesion (
	id INT PRIMARY KEY AUTO_INCREMENT,
	tipo VARCHAR(30) NOT NULL,
	fecha_ini_sesion datetime NOT NULL,
	fecha_fin_sesion datetime NOT NULL
);

CREATE TABLE profesor (
	id INT PRIMARY KEY AUTO_INCREMENT,
	nombre_prof VARCHAR(30) NOT NULL,
	cedula_prof VARCHAR(30) NOT NULL,
	telefono_prof VARCHAR(20),
	fecha_ing_profesor DATE NOT NULL,
	correo_prof VARCHAR(50),
	turno_prof VARCHAR(30) NOT NULL,
	sesion_id INT NOT NULL,
	FOREIGN KEY (sesion_id) REFERENCES sesion(id)
);

CREATE TABLE curso (
	id INT PRIMARY KEY AUTO_INCREMENT,
 	nombre_curs VARCHAR(50) NOT NULL,
 	fecha_creado DATE NOT NULL,
 	fecha_ini_curso DATE NOT NULL,
 	fecha_fin_curso DATE NOT NULL,
 	alumno_id INT NOT NULL,
 	profesor_id INT NOT NULL,
 	FOREIGN KEY (alumno_id) REFERENCES alumno(id),
 	FOREIGN KEY (profesor_id) REFERENCES profesor(id)
);




