-- Crear base de Datos
CREATE DATABASE airport;

CREATE TABLE flights (
	id INT AUTO_INCREMENT PRIMARY KEY,
	origin VARCHAR(255) NOT NULL,
	destination VARCHAR(255) NOT NULL,
	duration INT NOT NULL
)

CREATE TABLE passenger (
	id INT AUTO_INCREMENT PRIMARY KEY,
	name VARCHAR(255) NOT NULL,
	flights_id INT NOT NULL,
	FOREIGN KEY (flights_id) REFERENCES flights(id)
)

--Insetar datos en la tabla lights
INSERT INTO flights(origin,destination,duration)values("New York","London",415);
INSERT INTO flights(origin,destination,duration)values("Shanghai","Paris",760);
INSERT INTO flights(origin,destination,duration)values("Panama","Costa Rica",200);
INSERT INTO flights(origin,destination,duration)values("Mexico","Argentina",600);
INSERT INTO flights(origin,destination,duration)values("Instanbul","Tokyo",700);
INSERT INTO flights(origin,destination,duration)values("New York","Paris",435);
INSERT INTO flights(origin,destination,duration)values("Moscou","Paris",245);
INSERT INTO flights(origin,destination,duration)values("Lima","New York",435);

--insertar datos en la tabla passenger
INSERT INTO passenger(name, flights_id)values("Alice",1);
INSERT INTO passenger(name, flights_id)values("Bob",1);
INSERT INTO passenger(name, flights_id)values("Charlie",2);
INSERT INTO passenger(name, flights_id)values("Dave",2);
INSERT INTO passenger(name, flights_id)values("Erin",4);
INSERT INTO passenger(name, flights_id)values("Frank",6);
INSERT INTO passenger(name, flights_id)values("Grace",6);


--Query de consulta
SELECT origin, COUNT(*) FROM flights GROUP BY origin;
SELECT origin, COUNT(*) FROM flights GROUP BY origin HAVING COUNT(*) > 1

--Con esto ya tenemos dos tablas referenciadas, ahora si quisiéramos realizar una consulta a las tablas en un solo query, se utiliza la sentencia JOIN ejemplo:
SELECT origin, destination, name FROM flights JOIN passenger ON passenger.flights_id = flights.id

--Para complicarlo más podríamos decirle que solamente queremos el vuelo donde viajo Alice, utilizaríamos el siguiente query:

SELECT origin, destination, name FROM flights JOIN passenger ON passenger.flights_id = flights.id WHERE name = "Alice"

--En caso de que queramos solamente los datos que cumplen con esa condición y pertenezcan a la tabla flights podríamos utilizar la siguiente sentencia.

SELECT origin, destination, name FROM flights LEFT JOIN passenger ON passenger.flights_id = flights.id

--O queremos solamente los datos de esa condición que pertenezcan a la tabla Passenger, utilizaríamos este query:

SELECT origin, destination, name FROM flights RIGHT JOIN passenger ON passenger.flights_id = flights.id

