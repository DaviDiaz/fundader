CREATE DATABASE COVID19;
USE COVID19; 

/*---------------------------------------------------------
    Tabla Casos por Provincias y mes
---------------------------------------------------------*/
CREATE TABLE Provincias(
  ID varchar(20) PRIMARY KEY,
  Mes varchar(20),
  Bocas_del_Toro int,
  Cocle int,
  Colon int,
  Chiriqui int,
  Darien int,
  Herrera int,
  Los_Santos int,
  Panama int,
  Veraguas int,
  Panama_Oeste int
);

/*Insercion de datos*/

INSERT INTO Provincias(ID,Mes,Bocas_del_Toro,Cocle,
  Colon,Chiriqui,Darien,Herrera,Los_Santos,Panama,
  Veraguas,Panama_Oeste)
values("MAR20","Marzo",0,18,25,31,0,3,1,809,56,232);

INSERT INTO Provincias(ID,Mes,Bocas_del_Toro,Cocle,
  Colon,Chiriqui,Darien,Herrera,Los_Santos,Panama,
  Veraguas,Panama_Oeste)
values("ABR20","Abril",19,78,272,103,147,20,14,4172,272,1292);

INSERT INTO Provincias(ID,Mes,Bocas_del_Toro,Cocle,
  Colon,Chiriqui,Darien,Herrera,Los_Santos,Panama,
  Veraguas,Panama_Oeste)
values("MAY20","Mayo",190,91,475,350,211,17,18,8525,710,2558);

INSERT INTO Provincias(ID,Mes,Bocas_del_Toro,Cocle,
  Colon,Chiriqui,Darien,Herrera,Los_Santos,Panama,
  Veraguas,Panama_Oeste)
values("JUN20","Junio",1107,287,1416,1349,921,62,29,20816,1158,5838);

INSERT INTO Provincias(ID,Mes,Bocas_del_Toro,Cocle,
  Colon,Chiriqui,Darien,Herrera,Los_Santos,Panama,
  Veraguas,Panama_Oeste)
values("JUL20","Julio",2119,719,4133,3678,1278,259,120,36425,2172,11058);

/*---------------------------------------------------------
    Tabla Casos y Defunciones General
---------------------------------------------------------*/
CREATE TABLE General(
  General_id varchar(20) PRIMARY KEY,
  Mes varchar(20),
  Casos_positivos INT,
  Defunciones INT,
  FOREIGN KEY (General_id) REFERENCES Provincias(ID)
 );
 /*Insercion de datos*/
INSERT INTO General(General_id,Mes,Casos_positivos,Defunciones)
values("MAR20","Marzo",1181,30);

INSERT INTO General(General_id,Mes,Casos_positivos,Defunciones)
values("ABR20","Abril",5351,158);

INSERT INTO General(General_id,Mes,Casos_positivos,Defunciones)
values("MAY20","Mayo",6931,148);

INSERT INTO General(General_id,Mes,Casos_positivos,Defunciones)
values("JUN20","Junio",21000,295);

INSERT INTO General(General_id,Mes,Casos_positivos,Defunciones)
values("JUL20","Julio",29719,743);

/*---------------------------------------------------------
    VISTAS
---------------------------------------------------------*/
/*--------------------------------------------------------
    Vista Casos de Marzo
--------------------------------------------------------*/
CREATE OR REPLACE VIEW Marzo_provincia AS 
SELECT ID, MES, Bocas_del_Toro,Cocle,
  Colon,Chiriqui,Darien,Herrera,Los_Santos,Panama,
  Veraguas,Panama_Oeste 
  FROM Provincias
  WHERE ID = "MAR20";
  
/*--------------------------------------------------------
    Vista Casos de  Abril
--------------------------------------------------------*/
CREATE OR REPLACE VIEW Abril_provincia AS 
SELECT ID, MES, Bocas_del_Toro,Cocle,
  Colon,Chiriqui,Darien,Herrera,Los_Santos,Panama,
  Veraguas,Panama_Oeste 
  FROM Provincias
  WHERE ID = "ABR20";
  
/*--------------------------------------------------------
    Vista Casos de  Mayo
--------------------------------------------------------*/
CREATE OR REPLACE VIEW Mayo_provincia AS 
SELECT ID, MES, Bocas_del_Toro,Cocle,
  Colon,Chiriqui,Darien,Herrera,Los_Santos,Panama,
  Veraguas,Panama_Oeste 
  FROM Provincias
  WHERE ID = "MAY20";
  
/*--------------------------------------------------------
    Vista Casos de  Junio
--------------------------------------------------------*/
CREATE OR REPLACE VIEW Junio_provincia AS 
SELECT ID, MES, Bocas_del_Toro,Cocle,
  Colon,Chiriqui,Darien,Herrera,Los_Santos,Panama,
  Veraguas,Panama_Oeste 
  FROM Provincias
  WHERE ID = "JUN20";
  
/*--------------------------------------------------------
    Vista Casos de  Julio
--------------------------------------------------------*/
CREATE OR REPLACE VIEW Julio_provincia AS 
SELECT ID, MES, Bocas_del_Toro,Cocle,
  Colon,Chiriqui,Darien,Herrera,Los_Santos,Panama,
  Veraguas,Panama_Oeste 
  FROM Provincias
  WHERE ID = "JUL20";
